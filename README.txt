Retrieving Data from a Database
selectAll.sql

Use a SELECT to get all countries
selectCountries.sql

Use a SELECT to only get the country name, code, continent and region
selectCountryNameCodeContinentRegion.sql

Use a SELECT DISTINCT in an example
selectDistinct.sql

Use a WHERE clause to get all countries where the life expectancy is less than 50
whereLifeLessThan50.sql

Use a WHERE clause to get all countries where the life expectancy is less more than 60 
whereLifeMoreThan60.sql

Use a WHERE clause to get all countries where Life expectancy is greater than 75 and the population is more than 20 million
whereLifeMoreThan75AndPopulation.sql

Use a WHERE clause to get all countries where Life expectancy is greater than 70 and the population is 
less than 50 000, order the results by the life expectancy
whereLifeMoreThan70AndPopulationOrderBy.sql

Select all the counties where the name starts with an S or and N
countrySandN.sql

Select the Lowest and the Highest life expectancy
lowestAndHighest.sql

Count the number of countries per continent 
selectCountryContinent.sql

Get the average life expectancy of all countries
avarageLifeExpect.sql

Get the average life expectancy of countries in the African continent
avarageLifeExpectAfrica.sql
